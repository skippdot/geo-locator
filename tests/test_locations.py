# coding=utf-8
import unittest

from locator import Locator

try:
    import mock
except ImportError:
    from unittest import mock

import requests_mock


class TestLocations(unittest.TestCase):
    def setUp(self):
        self.locator = Locator()

    def test_bad_locations(self):
        bad_locations = ['', '-', 'Lviv', 'Ukraine', ]
        data_file = 'tests/results/bad_response.json'
        with requests_mock.Mocker() as mocker, open(data_file, 'r') as result_response:
            mocker.get(Locator.geocode_url, text=result_response.read())
            res = self.locator.process_location_list(bad_locations)
        self.assertListEqual(res, [])

    def test_locations_not_found(self):
        data_file = 'tests/results/bad_response.json'
        with requests_mock.Mocker() as mocker, open(data_file, 'r') as result_response:
            mocker.get(Locator.geocode_url, text=result_response.read())
            g = self.locator.process_location_list(['-', 'Kharkiv'])
            self.assertEqual(len(g), 0)

    def test_location_case_insensitive(self):
        data_file = 'tests/results/response.json'
        with requests_mock.Mocker() as mocker, open(data_file, 'r') as result_response:
            mocker.get(Locator.geocode_url, text=result_response.read())
            g = self.locator.process_location_list(['11 Sumska str, kharkiv', '11 sumska Str, kharkiV'])
            self.assertEqual(len(g), 1)
            self.assertEqual(g[0]['occurencies'], 2)

    def test_valid_locations(self):
        data_file = 'tests/results/response.json'
        with requests_mock.Mocker() as mocker, open(data_file, 'r') as result_response:
            mocker.get(Locator.geocode_url, text=result_response.read())
            g = self.locator.process_location_list([
                '11 Sumska str, kharkiv', '54 Pushkinska Str, Kharkiv', u'32 Nauky Ave, Харьков'
            ])
            self.assertEqual(len(g), 3)
            self.assertEqual(g[0]['occurencies'], 1)


if __name__ == '__main__':
    unittest.main()
