from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class LocationDBCache:
    def __init__(self, connection_string):
        self.db = create_engine(connection_string)
        Base.metadata.create_all(self.db)
        session_maker = sessionmaker(bind=self.db)
        self.session = session_maker()

    def fetch_location(self, location_name):
        return self.session.query(Location).filter(Location.location_name == location_name).first()

    def add_location(self, location_name, latlng):
        location = self.fetch_location(location_name)
        if not location:
            location = Location(location_name, latlng)
            self.session.add(location)
            self.session.commit()


class Location(Base):
    __tablename__ = "locations"

    id = Column(Integer, primary_key=True)
    location_name = Column(String, unique=True)
    latlng = Column(String)

    def __init__(self, location_name, latlng):
        self.location_name = location_name
        self.latlng = latlng
