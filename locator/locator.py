import logging
from collections import OrderedDict

import requests

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)


class Locator:
    api_key = None
    locations_cache = None
    geocode_url = 'https://maps.googleapis.com/maps/api/geocode/json1'

    def __init__(self, api_key=None, db_cache=None):
        self.api_key = api_key
        # cache manager to use, if provided
        self.locations_cache = db_cache

    def _query_geocode_api(self, location_name):
        params = {
            'address': location_name,
            'key': self.api_key,
        }

        r = requests.get(self.geocode_url, params=params)

        try:
            j = r.json()
            return j['results']
        except ValueError as exc:
            logger.debug("Wrong response from API")

        return []

    def locate_address(self, location_name):
        if self.locations_cache:
            location = self.locations_cache.fetch_location(location_name)
            if location:
                logger.info('Location information found for "%s". LatLng: %s', location.location_name, location.latlng)
                return {
                    'location_name': location.location_name,
                    'latlng': location.latlng
                }

        results = self._query_geocode_api(location_name)

        if len(results) > 0:
            result = results[0]
            geometry = result['geometry']
            location = geometry['location']
            latlng = "({}, {})".format(location['lat'], location['lng'])

            if geometry['location_type'] == "ROOFTOP" or ('partial_match' in result and result['partial_match']):
                logger.info('Location information found for "%s". LatLng: %s', location_name, latlng)
                if self.locations_cache:
                    self.locations_cache.add_location(location_name, latlng)

                return {
                    'location_name': location_name,
                    'latlng': latlng
                }

        logger.info('Location information for "%s" not found.', location_name)

        return None

    def process_location_list(self, locations):
        # preserve locations order
        output = OrderedDict()
        for location in locations:
            result = self.locate_address(location)
            if result:
                name = result['location_name'].lower()
                if name in output:
                    output[name]['occurencies'] += 1
                else:
                    result['occurencies'] = 1
                    output[name] = result
        return [output[x] for x in output]
