# coding=utf-8
import settings
from locator import Locator, LocationDBCache

db_cache = LocationDBCache(settings.database_connection_string)
loc = Locator(settings.api_key, db_cache)

locations = [
    '11 Sumska Str, Kharkiv',
    '11 sumska str, kharkiv',
    '11 Sumska str, kharkiv',
    '54 Pushkinska Str, Kharkiv',
    u'54 pushkinska str, kharkiv',
    '32 Nauky Ave, Kharkiv',
    '32 Nauky Ave, Kharkiv',
    '199 Klochkivska Street, Kharkiv',
    u'32 пр. Науки, Харьков',
    u'Сумская, 11, Kharkiv',
    u'54 Пушкинская, Харьков',
    '-',
    '',
]

result = loc.process_location_list(locations)

print(result)
