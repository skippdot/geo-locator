## Simple class for Google geocode API

For the example of how to use please check `main.py` file.

## Overview

To run the tests run following command:
```bash
python -m unittest discover
```
or simply run `tox`
